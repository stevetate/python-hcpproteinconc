﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Linq;
using System.Text;

namespace OneOmics.FoldChangeProcessing.Algorithms
{
	[TestFixture]
	public class TestSig
	{
		[Test]
		public void CreateReferenceDataSet()
		{
			var lines = File.ReadLines(@"c:\temp\sigmoidInput.txt");
			var doubles = lines.Select(double.Parse).ToArray();
			var output = SigmoidMappedWeights.Calculate(doubles, SigmoidMappedWeights.InputType.Area, 8000, 1000, 500000, double.NaN);
			var sb = new StringBuilder(13500);
			foreach (var d in output)
				sb.AppendLine(d.ToString(CultureInfo.InvariantCulture));
			File.WriteAllText(@"c:\temp\sigmoidOutput.txt", sb.ToString());
		}
	}

	public static class SigmoidMappedWeights
	{
		public enum InputType
		{
			Area,
			SignalToNoise
		}

		public static double[] Calculate(double[] x, InputType inputType, double noiseThreshold, double xMinimum,
			double xMaximum, double slope)
		{
			var data = new double[x.Length];
			switch (inputType)
			{
				case InputType.Area:
					Parallel.For(0, data.Length, i => { data[i] = Math.Log(x[i] + 1); });
					noiseThreshold = Math.Log(noiseThreshold);
					xMaximum = Math.Log(xMaximum);
					xMinimum = Math.Log(xMinimum);
					break;
				case InputType.SignalToNoise:
					Parallel.For(0, data.Length, i => { data[i] = x[i] + 1e-5; });
					break;
			}

			data.Clamp(xMinimum, xMaximum);

			PerformSharpSigmoid(data, xMinimum, xMaximum, noiseThreshold);

			return data;
		}

		private static void PerformSharpSigmoid(IList<double> inputs, double xMinimum, double xMaximum, double noiseThreshold)
		{
			var shiftedMinimum = (xMinimum - noiseThreshold) / (1 + Math.Abs(xMinimum - noiseThreshold));
			var shiftedMaximum = (xMaximum - noiseThreshold) / (1 + Math.Abs(xMaximum - noiseThreshold));
			Parallel.For(0, inputs.Count,
				i => { inputs[i] = SingleSharpSigmoid(inputs[i], noiseThreshold, shiftedMinimum, shiftedMaximum); });
		}

		private static double SingleSharpSigmoid(double x, double noiseThreshold, double min, double max)
		{
			const double midpoint = 0.5d;
			var result = (x - noiseThreshold) / (1 + Math.Abs(x - noiseThreshold));
			result -= min;
			result /= (max - min);
			if (result < midpoint)
			{
				result *= Math.Pow(midpoint + result, 3);
			}
			return result;
		}

		public static double OrigConfidenceCalc(double yy, int n)
		{
			// n is mean between number of samples in each comparison. 
			// yy is intermediate confidence (ratio)
			const double cThreshold = 0.8d;
			const double multiplier = cThreshold * Math.PI * 2.0d;

			double aThreshold;
			double dThreshold;
			GetThresholds(n, out aThreshold, out dThreshold);

			var result = 0.0d;
			if (yy > 0)
			{
				var aa = Math.Atan((yy - aThreshold) * multiplier) / 0.9d;
				var aa1 = Math.Atan((0.2d - aThreshold) * multiplier) / 0.9d;
				var aa2 = Math.Atan((dThreshold - aThreshold) * multiplier) / 0.9d;
				aa -= aa1;
				aa2 -= aa1;
				result = aa / aa2 / 0.975d;
				result = result.Clamp(0, 1);
			}
			return result;
		}

		public static void GetThresholds(int n, out double aThreshold, out double dThreshold)
		{
			switch (n)
			{
				case 0:
					aThreshold = 1.0;
					dThreshold = 20.0;
					break;
				case 1:
					aThreshold = 0.98;
					dThreshold = 11.0;
					break;
				case 2:
					aThreshold = 0.95;
					dThreshold = 9.0;
					break;
				case 3:
					aThreshold = 0.91;
					dThreshold = 4.0;
					break;
				case 4:
					aThreshold = 0.89;
					dThreshold = 3.5;
					break;
				case 5:
					aThreshold = 0.87;
					dThreshold = 2.7;
					break;
				case 6:
					aThreshold = 0.855;
					dThreshold = 2.4;
					break;
				case 7:
					aThreshold = 0.84;
					dThreshold = 2.2;
					break;
				case 8:
					aThreshold = 0.83;
					dThreshold = 2.0;
					break;
				case 9:
					aThreshold = 0.79;
					dThreshold = 1.8;
					break;
				default:
					aThreshold = 0.75;
					dThreshold = 1.8;
					break;
			}
		}
	}
}