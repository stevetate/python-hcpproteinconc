import pandas as pd

class mlr(object):

    @staticmethod
    def mlrMedian(data, columns):
        result = pd.DataFrame()

        for c in columns:
            result[c]=data[c].mean(axis=1)

        return result