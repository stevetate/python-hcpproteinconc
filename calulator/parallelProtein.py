from joblib import Parallel, delayed
import multiprocessing
import pandas as pd
import filteredProteinSum as fPS
import numpy as np
import threading

dx=pd.DataFrame()

def parrallelGetProteins(proteins, samples, controlG, control):
    ex = []
    # for s in samples:
    #     ex.append(s['experimental_group'])
    # uniqueExperimental = list(set(ex))
    cpus = multiprocessing.cpu_count()
    print ("number of CPU's which have been found")
    print cpus

    res = pd.DataFrame()
    threading.current_thread().name = 'MainThread'
    res = res.append(Parallel(n_jobs=cpus, backend='multiprocessing', verbose=5)(delayed(itt)(x, controlG, samples, control) for x in proteins))
    #threading.current_thread().name = 'waitress'
    #res = res.append(Parallel(n_jobs=2, backend='threading', verbose=5)(delayed(itt)(x, controlG, samples, control) for x in proteins))
    return res #uniqueExperimental

def itt(x, controlG, uniqueE, control): # issue in here with the columns being switched as the data is moving through ?
    pro = pd.DataFrame()
    #mlr = pd.DataFrame()
    for p in x['peptide_areas']:
        df = pd.DataFrame((p['normalized_response_per_sample']))
        #dt = pd.DataFrame((p['mlrs']))
        df.columns = {p['sequence']}
        #dt.columns = {p['sequence']}
        df = df.transpose()
        #dt = dt.transpose()
        df.columns = uniqueE
        #dt.columns = uniqueE
        df['MlrWeight']= np.median(np.array(p['mlrs']))
        pro = pro.append(df)
        #mlr = mlr.append('MlrWeight' )

    protein = fPS.proteinSum.peptideMean(pro, uniqueE)
    protein['MlrWeight'] = pro['MlrWeight']# mlrM.mlr.mlrMedian(mlr, controlG)[control] removed due to simplifcation of the json input
    protein['protein'] = x['accession']
    protein['length'] = x['protein_sequence_length']
    res = fPS.proteinSum.sumProtein(protein, 3, control, uniqueE)
    return res
    #dx.append(res)
