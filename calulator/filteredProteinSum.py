import pandas as pd


class proteinSum(object):

    @staticmethod
    def proteinSum(data, proteinList, colname, numPept):

        for k in data.index.unique():
            temp = pd.DataFrame(data.loc[k])
            try:
                temp.sort_values(["MlrWeight", colname], inplace=True, ascending=False)
                pepsum = sum(temp[colname].head(numPept))
                proteinList.loc[k, colname] = pepsum
            except:
                transposedTemp = temp.transpose()
                if transposedTemp['MlrWeight'][0] > 0.5:
                    proteinList.loc[k, colname] = transposedTemp['MlrWeight'][0]
                else:
                    proteinList.loc[k, colname] = 0.0001
        return proteinList

    @staticmethod
    def peptideMean(data, columns):
        result = pd.DataFrame()

        for c in columns:
            result[c]=data[c].mean(axis=1)

        return result

    @staticmethod
    def sumProtein(data, numPeptide, load, experimentalGroups):
        result = pd.DataFrame()
        result['protein']=[data['protein'][0]]
        result['length']=[data['length'][0]]
        try:
            data.sort_values(['MlrWeight', load], inplace=True, ascending=False)
            for group in experimentalGroups:
                pepSum = sum(data[group].head(numPeptide))
                result[group] = [pepSum]

        except:
            print ('error')

        return result
