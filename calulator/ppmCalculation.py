import numpy as np
import pandas as pd

class ppmCalculation(object):
    """A class to determine the ppm concetration of a proteins in thh ranked list of the proteins

    """

    @staticmethod
    def ppm(data, TotalProteinConc, column):
        cell = 'ppm' + column
        data[cell] = np.NaN
        for k in data.index.unique():
            try:
                data.loc[k, cell] = data.loc[k, column] / TotalProteinConc
            except:
                data.loc[k, cell] = 0.0
                print (k)
        return data

    @staticmethod
    def ppm(data, Tpv, order):
        proteinppm = pd.DataFrame()
        data.transpose()
        data = data.set_index(data['protein'])
        for index, row in data.iterrows():
            PC= ((row[0:len(row)-1].transpose()/Tpv.astype(float))) # added modification to remove Nan values
            PC.transpose()
            PC['protein']=row['protein']
            proteinppm = proteinppm.append(PC, ignore_index=True)

        proteinppm=proteinppm[order]
        proteinppm['protein']=data.index
        return proteinppm
