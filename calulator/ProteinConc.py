import numpy as np
import pandas as pd

class proteinConcentration(object):
    """ Class to create a protein concentration from known inputs of a calibration protein

    Attributes:

    """

    @staticmethod
    def calcProteinConc(expPro, targetlist, colname, controlArea, sampleV, controlConc):
        # type: (object, object, object, object, object, object) -> object
        expPro[colname] = np.NaN
        for k in targetlist.index:
            targetArea = targetlist.loc[k, colname]
            targetLength = targetlist.loc[k, 'length']
            try:
                targetConc = ((targetArea / controlArea) * controlConc * (targetLength * 100)) / sampleV
            except:
                targetConc = 0.00001
            expPro.loc[k, colname] = targetConc
        return

    @staticmethod
    def calcProteinC(data, sampleV, controlConc, experimentalGroup, controlProtien='sp|P0000|ENOA_HUMAN'):

        proteinConc=pd.DataFrame()
        data = data.set_index('protein')

        for index, row in data.iterrows():

            PC= ((row[1:len(row)].transpose() / data.loc[controlProtien][1:len(row)].transpose()) * controlConc.astype(int) * int(row['length'])) /sampleV.astype(int)
            PC.transpose()
            PC['protein']=index
            proteinConc = proteinConc.append(PC, ignore_index=True)

        proteinConc=proteinConc[experimentalGroup]
        proteinConc['protein']=data.index
        return proteinConc


