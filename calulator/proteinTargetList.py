import pandas as pd


class createProteinList(object):
    @staticmethod
    def createProteinListinTargetSample(targetlist, proteins, colname):
        proData = pd.DataFrame(columns=('protein', 'length', colname))

        for k in targetlist.index.unique():
            proData.loc[len(proData)] = [k, proteins.loc[k][0], 0]

        proData.set_index(proData['protein'], inplace=True)
        return proData


