import json
import pandas as pd
import logging
import socket
from logging.handlers import  SysLogHandler

from calulator import filteredProteinSum as fPS
from calulator import ProteinConc as pC
from calulator import mlrMedian as mlrM
from calulator import parallelProtein as pP
from calulator import ppmCalculation as ppm


class jsonHandler(object):
    # vd = {'LDpc': 0.65, 'LDsv': 20, 'LDeno': 40, 'VIIIpc': 0.13, 'VIIIsv': 700, 'VIIIeno': 10, 'POpc': 0.19,
    #       'POsv': 526, 'POeno': 10, 'CQpc': 0.12, 'CQsv': 700, 'CQeno': 10, 'DSpc': 5, 'DSsv': 20, 'DSeno': 5}

    proteinConc = [0.65, 0.12, 0.13, 5.0, 0.19]

    # / Users / stephentate / SourceCode / PythonHCP / testData / Biogen_smallData.json
    def readJSON(self, response):
        if response == None:
            path = '/home/app/testData/hcp-sample-data.json'
            JD = open(path).read()
        else:
            JD = response


        samples = json.loads(JD)['data']['samples']
        proteins = json.loads(JD)['data']['proteins']
        controlGroups = json.loads(JD)['data']['result_group_names']
        calibrationProtien = json.loads(JD)['data']['calibration_protein']
        loadGroup = json.loads(JD)['data']['load_group']

        return samples, proteins, controlGroups, calibrationProtien, loadGroup

    def process(self, response):

        calibrationProtein = "sp|O14672|ADA10_HUMAN"

        print('ReadingJson')
        samples, protein, controlGroups, calibrationProtein, loadGroup = self.readJSON(response)
        print('getting control groups')
        controlGroups, experimentGroup = self.getcontrolGroups(controlGroups)
        # print('getting data for each protein')
        # #results, uniqueExperimental = self.getProteinData(proteins=protein, samples=sample, controlG=controlGroups)
        print('getting different meta data items')
        metaSimple, metaFull = self.handleMetaData(samples)
        print('getting protein all together')
        results = pP.parrallelGetProteins(proteins=protein, samples=metaFull.index.values, controlG=controlGroups,
                                          control=loadGroup)
        # the line below has example values added the 'enoa' is the accession of the control protien, 10 and 50 are the
        # control concentraions and samples volumes -- both are arrays in the data and are used in order as defined
        # in the input json.
        print('determining protein conc')
        # manage the different meta data for the proteins of interest in the samples this needs to come direct from the json
        # which is not completed

        results = pC.proteinConcentration.calcProteinC(results, metaSimple['SampleVolume'],
                                                       metaSimple['ControlProteinConc'], metaSimple.index.values,
                                                       controlProtien=calibrationProtein)
        #results.to_csv('results3peptides_proteinConcg.csv', sep='\t')
        print('getting ppm levels')
        data = ppm.ppmCalculation.ppm(results, metaSimple['SampleProteinLevel'], metaSimple.index.values)
        jsonout = " null sent"
        if response == None:
            data.to_csv('results3peptides_ppmg.csv', sep='\t')
            jsonout= data.to_json(orient='records')
        else:
            jsonout = data.to_json(orient='records')

        return jsonout

    def getcontrolGroups(self, controlG):
        con = []
        exp = []
        #Altered to grab first item this is the control group
        for grp in controlG:
            con.append(grp.split(" ")[0])
            exp.append(grp.split(" ")[2])

        return con, exp

    def handleMetaData(self, samples):
        meta = ['Experiment Group', 'ControlProteinConc', 'SampleVolume',
                'SampleProteinLevel']  # this includes a hard coded meta data for the samples could be made dynamics but.
        metaResult = pd.DataFrame(
            columns={'SampleProteinLevel', 'SampleVolume', 'ControlProteinConc', 'Experiment Group'})
        for sample in samples:
            x = []
            for y in sample['meta_data']:
                if y['attribute'] == meta[0]:
                    print y['attribute']
                    print y['group']
                    x.append(y['group'])
                if y['attribute'] == meta[1]:
                    print y['attribute']
                    print y['group']
                    x.append(y['group'])
                if y['attribute'] == meta[2]:
                    print y['attribute']
                    print y['group']
                    x.append(y['group'])
                if y['attribute'] == meta[3]:
                    print y['attribute']
                    print y['group']
                    x.append(y['group'])
            metaResult.loc[len(metaResult)] = x
        metaFull = metaResult.set_index('Experiment Group')
        metaSimple = metaResult.drop_duplicates().set_index('Experiment Group')
        return metaSimple, metaFull

    def getProteinData(self, proteins, samples, controlG):

        ex = []
        for s in samples:
            ex.append(s['experimental_group'])
        uniqueExperimental = list(set(ex))

        dx = pd.DataFrame()

        for x in proteins:
            pro = pd.DataFrame()
            mlr = pd.DataFrame()
            for p in x['peptide_areas']:
                df = pd.DataFrame((p['normalized_responses_per_sample']))
                dt = pd.DataFrame((p['mlrs']))
                df.columns = {p['sequence']}
                dt.columns = {p['sequence']}
                df = df.transpose()
                dt = dt.transpose()
                df.columns = ex
                dt.columns = controlG
                pro = pro.append(df)
                mlr = mlr.append(dt)

            protein = fPS.proteinSum.peptideMean(pro, uniqueExperimental)
            protein['MlrWeight'] = mlrM.mlr.mlrMedian(mlr, controlG)['LO']
            protein['protein'] = x['accession']
            protein['length'] = x['protein_sequence_length']
            res = fPS.proteinSum.sumProtein(protein, 3, 'LO', uniqueExperimental)

            dx = dx.append(res)
        return dx, uniqueExperimental

    def __init__(self):
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        f = ContextFilter()
        logger.addFilter(f)
        syslog = SysLogHandler(address=('logs2.papertrailapp.com', 47332))
        formatter = logging.Formatter('%(asctime)s + hcp-v4-calc-%(hostname)s YOUR_APP: %(message)s', datefmt='%b %d %H:%M:%S')
        syslog.setFormatter(formatter)
        logger.addHandler(syslog)
        logger.info("init complete and loggin on")


class ContextFilter(logging.Filter):
    hostname= socket.gethostname()

    def filter(self, record):
        record.hostname = ContextFilter.hostname
        return True