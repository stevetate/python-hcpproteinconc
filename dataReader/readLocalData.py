import pandas as pd

from transition.sigmoidMapper import SigmoidMappedWeights


class readLocalData(object):
    """A class object which reads the local test files and create pandas dataframes for the data

    """

    @staticmethod
    def readproteincsv():
        proteindata = pd.DataFrame.from_csv(
            "/Users/stephentate/Documents/SourceCode_web/python-HCPProteinConc/testData/BiogenProteinData.txt", sep=',')
        return proteindata

    @staticmethod
    def readpeptidecsv():
        peptidedata = pd.DataFrame.from_csv(
            "/Users/stephentate/Documents/SourceCode_web/python-HCPProteinConc/testData/PQD_mlr.txt", sep='\t')
        return peptidedata

    @staticmethod
    def slicepeptideData(data):
        base = data[data.columns[0:5]]
        cq = data.filter(regex="nCQ.*")
        np = data.filter(regex="nP.*")
        nl = data.filter(regex="nL.*")
        nDS = data.filter(regex="nDS.*")
        nvi = data.filter(regex="nviii.*")

        # dt = pd.DataFrame(cq.mean(axis=1), cq)
        cq['CQmean'] = cq.mean(axis=1)
        np['POmean'] = np.mean(axis=1)
        nl['LDmean'] = nl.mean(axis=1)
        nDS['DSmean'] = nDS.mean(axis=1)
        nvi['VIIImean'] = nvi.mean(axis=1)
        cq = pd.concat([base, cq], axis=1)
        np = pd.concat([base, np], axis=1)
        nl = pd.concat([base, nl], axis=1)
        base = pd.concat([base, cq['CQmean'], np['POmean'], nl['LDmean'], nDS['DSmean'], nvi['VIIImean']], axis=1)
        # filter data to remove low intensity peptides <1000
        cq.drop(cq[cq['CQmean'] < 500].index, inplace=True)
        np.drop(np[np['POmean'] < 500].index, inplace=True)
        nl.drop(nl[nl['LDmean'] < 500].index, inplace=True)

        return base

    @staticmethod
    def readTransitionAreas():
        trAreas = pd.DataFrame.from_csv("/Users/stephentate/Documents/SourceCode_web/python-HCPProteinConc/testData/TransitionQuantData-[Area].txt", sep='\t')
        return trAreas

    @staticmethod
    def trDataConversion(data):
        base = data[data.columns[0:6]]
        cq = data.filter(regex="CQ.*")
        np = data.filter(regex="Por.*")
        nl = data.filter(regex="Loa.*")
        nDS = data.filter(regex="DS.*")
        nvi = data.filter(regex="VIII.*")
        base = pd.concat([base, cq, np, nl, nDS, nvi], axis=1)
        sm = SigmoidMappedWeights()
        sm.Calculate(base[7:], "Area",8000,1000,500000)
        return



