FROM sciex/docker-base-python-image:2017.02.22
# Install Python
RUN \
  apt-get update -y
# Clean up APT when done.
RUN sudo apt-get -y install wget
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV SERVICE_NAME HCPppm


ADD . /home/app
WORKDIR /home/app

RUN pip install .

EXPOSE 80

CMD ["sbin/my_init", "--", "python", "/home/app/hcpapi/main.py"]

CMD "python", "hcpapi/main.py"

ENTRYPOINT ["python", "hcpapi/main.py", "&"]

