from IPython.core.display import Math
import numpy as np
import pandas as pd


class SigmoidMappedWeights(object):
    #public enum InputType
    #{
    #	Area,
    #	SignalToNoise
    #}

    ## this assumes that the input X is a single column of values

    def Calculate(self, x, inputType,  noiseThreshold,  xMinimum,  xMaximum):

        data = pd.DataFrame

        if inputType == "Area":
            data = np.log(x)
            noiseThreshold = np.log(noiseThreshold)
            xMaximum = np.log(xMaximum)
            xMinimum = np.log(xMinimum)
        elif inputType == "SignalToNoise":
            i=0
        data = data.clip(xMinimum, xMaximum)
        #np.clip(data, xMinimum, xMaximum)
        data = self.PerformSharpSigmoid(data, xMinimum, xMaximum, noiseThreshold)
        data = data.clip(0, 1)
        return data


    def PerformSharpSigmoid(self, inputs,  xMinimum,  xMaximum,  noiseThreshold):

        shiftedMinimum = (xMinimum - noiseThreshold) / (1 + np.abs(xMinimum - noiseThreshold))
        shiftedMaximum = (xMaximum - noiseThreshold) / (1 + np.abs(xMaximum - noiseThreshold))
        inputs = inputs.applymap(lambda x: self.SingleSharpSigmoid(x, noiseThreshold, shiftedMinimum, shiftedMaximum))
        return inputs

    def SingleSharpSigmoid(self, x, noiseThreshold, min, max):
        midpoint = 0.5
        result = (x - noiseThreshold) / (1 + np.abs(x - noiseThreshold))
        result = result - min
        result = result / (max - min)
        if (result < midpoint):
            result = result * np.power(midpoint + result, 3)

        return result


class SigmoidTest(object):
    def tr(self):
        p = pd.DataFrame.from_csv('/Users/stephentate/Documents/SourceCode_web/python-HCPProteinConc/sigmoid_test/sigmoidInput.txt', index_col=None, header=None)

        t = SigmoidMappedWeights()
        pt = t.Calculate(p, "Area",  8000, 1000, 500000)
        print pt

    # def OrigConfidenceCalc(yy, n):
    #
    # 			// n is mean between number of samples in each comparison.
    # 			// yy is intermediate confidence (ratio)
    # cThreshold = 0.8d;
    # multiplier = cThreshold * Math.PI * 2.0d;
    #
    # aThreshold;
    # dThreshold;
    # 			GetThresholds(n, out aThreshold, out dThreshold);
    #
    # 			var result = 0.0d;
    # 			if (yy > 0)
    # 			{
    # 				var aa = Math.Atan((yy - aThreshold) * multiplier) / 0.9d;
    # 				var aa1 = Math.Atan((0.2d - aThreshold) * multiplier) / 0.9d;
    # 				var aa2 = Math.Atan((dThreshold - aThreshold) * multiplier) / 0.9d;
    # 				aa -= aa1;
    # 				aa2 -= aa1;
    # 				result = aa / aa2 / 0.975d;
    # 				result = result.Clamp(0, 1);
    # 			}
    # 			return result;
    # 		}
    #
    # 		public static void GetThresholds(int n, out double aThreshold, out double dThreshold)
    # 		{
    # 			switch (n)
    # 			{
    # 				case 0:
    # 					aThreshold = 1.0;
    # 					dThreshold = 20.0;
    # 					break;
    # 				case 1:
    # 					aThreshold = 0.98;
    # 					dThreshold = 11.0;
    # 					break;
    # 				case 2:
    # 					aThreshold = 0.95;
    # 					dThreshold = 9.0;
    # 					break;
    # 				case 3:
    # 					aThreshold = 0.91;
    # 					dThreshold = 4.0;
    # 					break;
    # 				case 4:
    # 					aThreshold = 0.89;
    # 					dThreshold = 3.5;
    # 					break;
    # 				case 5:
    # 					aThreshold = 0.87;
    # 					dThreshold = 2.7;
    # 					break;
    # 				case 6:
    # 					aThreshold = 0.855;
    # 					dThreshold = 2.4;
    # 					break;
    # 				case 7:
    # 					aThreshold = 0.84;
    # 					dThreshold = 2.2;
    # 					break;
    # 				case 8:
    # 					aThreshold = 0.83;
    # 					dThreshold = 2.0;
    # 					break;
    # 				case 9:
    # 					aThreshold = 0.79;
    # 					dThreshold = 1.8;
    # 					break;
    # 				default:
    # 					aThreshold = 0.75;
    # 					dThreshold = 1.8;
    # 					break;

s = SigmoidTest()
s.tr()
