from distutils.core import setup

setup(
    name='hcpapi',
    version='0.0.1',
    packages=['calulator', 'dataReader', 'transition', 'hcpapi'],
    url='',
    license='copyright Sciex 2017',
    author='stephen.tate@sciex.com',
    author_email='',
    description='determines the HCP PPM levels in the data from oneomics',
    install_requires=[
        'pandas',
        'numpy',
        'multiprocessing',
        'joblib',
        'flask',
        'flask_restful',
        'waitress',
        'cython',
    ]
)
