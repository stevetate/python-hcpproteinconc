from flask import Flask, request, Response, jsonify
from flask_restful import Resource, Api
from waitress import serve
from dataReader import jsonReader
import threading
app = Flask(__name__)


#Subclass flask to add the obo ontology dictionary to the webapp load step
class FlaskApp(Flask):
    def __init__(self, *args, **kwargs):
        super(FlaskApp, self).__init__(*args, **kwargs)

class ppmcalc(Resource):
    def post(self):
        try:
            jInput=request.data
            jx = jsonReader.jsonHandler()
            data = jx.process(jInput)
            resp = Response(response=data, status=200, mimetype="application/json")
        except Exception as e :
            resp = Response(response='helloworld OOPs there has been an error !', status=400)

        return resp


    def get(self):
        try:
            jInput=request.data
            jx = jsonReader.jsonHandler()
            data = jx.process(jInput)
            resp = Response(response=data, status=200, mimetype="application/json")
        except Exception as e :
            resp = Response(response='helloworld -- im here on startup', status=400)
        return resp

class webTest(Resource):
    def post(self):
        jx = jsonReader.jsonHandler()
        data = jx.process(None)
        resp = Response(response=data, status=200)
        return resp
    def get(self):
        jx = jsonReader.jsonHandler()
        data = jx.process(None)
        resp = Response(response=data, status=200)
        return resp


app = FlaskApp(__name__)
api = Api(app)

todos = {}
api.add_resource(ppmcalc, '/')
api.add_resource(webTest, '/test')
#api.add_resource(Go, '/go')

if __name__ == '__main__':
    serve(app, port=8081)