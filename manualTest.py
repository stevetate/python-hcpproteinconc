import re

import ProteinConc as pc
import matplotlib.pyplot as plt
import pandas as pd
import ppmCalculation as ppm
import proteinTargetList as pTL
from __builtin__ import len

from calulator import filteredProteinSum as fpSum
from dataReader import readLocalData as rld

##global Biogen Values
vd = {}
vd['LDpc'] = 0.65  #
vd['LDsv'] = 20
vd['LDeno'] = 40
vd['VIIIpc'] = 0.13
vd['VIIIsv'] = 700
vd['VIIIeno'] = 10
vd['POpc'] = 0.19
vd['POsv'] = 526
vd['POeno'] = 10
vd['CQpc'] = 0.12
vd['CQsv'] = 700
vd['CQeno'] = 10
vd['DSpc'] = 5
vd['DSsv'] = 20
vd['DSeno'] = 5


def graph(x, y):
    plt.figure()
    plt.scatter(x, y, c='blue', alpha=0.1, edgecolors='none')
    plt.yscale('log')
    #plt.xscale('log')
    plt.show()

def getCalibrantProtein(targetlist, name):
    return targetlist.loc[name, 'sumMean']


def PrimaryLoop(baseData):
    for x in range(5, len(base.columns)):
        # filter data to remove those peptides with intensity less than X counts
        subset = base.drop(base[base[base.columns[x]] < 300].index)
        print('sample data = {} :subset={} and base = {}'.format(base.columns[x], len(subset), len(base)))

        # get column name which is experiment in this case will need changing for correct data
        cname = re.sub("mean", "", base.columns[x])
        colname = cname + 'mean'
        sampleVol = cname + 'sv'
        enoConc = cname + 'eno'
        conTV = cname + 'pc'

        # create a model which is the filtered data but confirmed against main protein list
        experimentSpecific = pTL.createProteinList.createProteinListinTargetSample(subset, pro, colname)

        # Get Sum of the peptides from the most reproducible
        fpSum.proteinSum.proteinSum(subset, experimentSpecific, colname, 2)

        # grab the control protein value
        controlA = experimentSpecific.loc['sp|P0000|ENOA_HUMAN', colname]

        pc.proteinConcentration.calcProteinConc(experimentalProteins, experimentSpecific, colname, controlA,
                                                vd[sampleVol], vd[enoConc])

        ppm.ppmCalculation.ppm(experimentalProteins, vd[conTV], colname)
        # print(len(samplespecific))
        # print (samplespecific)


## start here
#transData = rld.readLocalData.readTransitionAreas()
#transData = rld.readLocalData.trDataConversion(transData)
# get peptide data
pepData = rld.readLocalData.readpeptidecsv()  # readpeptidecsv()

# get protein data here
pro = rld.readLocalData.readproteincsv()

# slice data into appropriate information
base = rld.readLocalData.slicepeptideData(pepData)

# grab the list of proteins in the experiment for the final results table
experimentalProteins = pd.DataFrame(pro.ix[:, 0])

# run primary loop
PrimaryLoop(base)

#ENSURE THAT SAME ORDERING IS IDENTICAL !!!!


print(experimentalProteins)
experimentalProteins.to_csv('results3peptides.csv', sep='\t')
i = 0
# 049 = {str} 'sp|P0000|ENOA_HUMAN'

# 048 = {str} 'sp|P00001|ENOA_HUMAN'

# for k in proData.index:
#    targetArea = proData.loc[k,'sumMean']
#    targetLength = proData.loc[k,'length']
#    targetConc = protein(targetArea, controlArea, targetLength, CQsv, CQeno)
#    #print(proData.loc[k,'sumMean'])
#    print(targetConc)
#    proData.loc[k,'conc']=targetConc


# print(proData)
# graph(cq['mean'], cq['MlrWeight'])
# graph(fp['mean'], fp['MlrWeight'])
# graph(lo['mean'], lo['MlrWeight'])
